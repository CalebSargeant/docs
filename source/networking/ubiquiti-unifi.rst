Ubiquiti UniFi
==============

Controller
----------

Windows Credential Recovery
^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Open Ubiquiti UniFi Controller.
2. Download mongoDB. https://www.mongodb.com/download-center
3. Open CMD
4. ``cd %userprofile%\downloads\mongo..\bin``
5. ``/mongo --port 27117``
6. ``use ace``
7. ``db.admin.find()``

The username and password will be at the bottom
