Genie
=====

Automatically parses device configuration into yaml, json, etc. to be used with python, for example.

https://developer.cisco.com/docs/genie-docs/

.. code-block:: bash

  genie learn interface --testbed testbed.yaml --device iosxe --output learn

  
