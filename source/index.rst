.. Caleb Sargeant's Notes documentation master file, created by
   sphinx-quickstart on Tue Mar  3 09:35:04 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Caleb Sargeant's documentation!
==========================================

A place for all of my technical how-to guides, documentation, study notes, random notes and things I jot down, etc.

Table of Contents
-----------------

.. toctree::
  :maxdepth: 2
  :caption: Networking

  networking/security/index
  networking/cisco/index
  networking/fortigate
  networking/hp
  networking/juniper
  networking/ubiquiti-unifi

.. toctree::
  :maxdepth: 2
  :caption: Computing

  computing/ansible/index
  computing/containerisation/index
  computing/linux/index
  computing/python/index
  computing/microsoft/index

.. toctree::
  :maxdepth: 2
  :caption: Other

  other/api/index
  other/general/index
  other/iperf

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
